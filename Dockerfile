ARG  PYTHON_VERSION=3.9.1
FROM python:$PYTHON_VERSION-slim-buster

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt-get update && \
    apt-get -y install locales && \
    localedef -f UTF-8 -i ja_JP ja_JP.UTF-8
ENV LANG ja_JP.UTF-8
ENV LANGUAGE ja_JP:ja
ENV LC_ALL ja_JP.UTF-8
ENV TZ JST-9

RUN apt-get install -y vim less make curl git
RUN pip install --upgrade pip

#mkdocs install
RUN python3 -m pip install \
    click==8.0.0a1 \
    future==0.18.2 \
    jinja2==2.11.3 \
    joblib==1.0.1 \
    livereload==2.6.3 \
    lunr[languages]==0.5.8 \
    markdown==3.3.3 \
    markupsafe==2.0.0a1 \
    mkdocs-material-extensions==1.0.1 \
    mkdocs-material==6.2.8 \
    mkdocs==1.1.2 \
    nltk==3.5 \
    prompt-toolkit==3.0.16 \
    pygments==2.7.4 \
    pymdown-extensions==8.1.1 \
    python-markdown-math==0.8 \
    pyyaml==5.4.1 \
    regex==2020.11.13 \
    six==1.15.0 \
    tornado==6.1 \
    tqdm==4.56.2 \
    wcwidth==0.2.5 \
    plantuml-markdown==3.4.2 \
    mdx_truly_sane_lists==1.2 \
    mkdocs-exclude==1.0.2 \
    markdown-katex==202009.1026


RUN pip3 check
RUN pip3 freeze

VOLUME /root/workspace
WORKDIR /root/workspace
EXPOSE 8000

CMD ["/bin/bash"]
