# mkDocs-material-darani

mkdocs-material-boilerplate  
https://github.com/peaceiris/mkdocs-material-boilerplate

上記Projectを参考に、個人用にカスタマイズ

## サンプルのテスト実行（ローカルでserve起動する場合）
```
$ docker-compose up
```

## Pages
GitlabでCI/CDパイプラインでmaster登録時にPagesを公開する仕組みを導入済み  
以下で公開中
https://sisinn.gitlab.io/mkdocs-material-darani

## 残課題
メニューが表示されない・・・
