第１層を書かなれば、ymlのナビゲーションの記載が使われる。

## テキスト装飾 {#custom-id}

注意：日本語を就職する場合は、半角スペースが必要（VSCodeではなしでも修飾される）  
これは *イタリック* です  
これは **ボールド** です  
これは ***イタリック＆ボールド*** です  
これは ~~打消し線~~ です  
これは<sup>上付き</sup>です  
これは<sub>下付き</sub>です

## jump-sample  
見出しのジャンプ動作  
[to header1](#header1)

## リスト変換

### Ul 箇条書きリスト

ハイフン`-`、プラス`+`、アスタリスク`*`のいずれかを先頭に記述します。  
ネストはインデントで表現。

* リスト1
  * リスト1_1
    * リスト1_1_1
    * リスト1_1_2
  * リスト1_2
* リスト2
* リスト3

### Ol 番号付きリスト

`番号.`を先頭に記述します。ネストはタブで表現。  
番号は自動的に採番されるため、すべての行を1.と記述するのがお勧め。

1. 番号付きリスト1
   1. 番号付きリスト1-1
   1. 番号付きリスト1-2
1. 番号付きリスト2
1. 番号付きリスト3


1. First ordered list item

   Second paragraph of first item.

1. Another item

## Hr 水平線

アンダースコア`_` 、アスタリスク`*`、ハイフン`-`などを3つ以上連続して記述します。ハイフン使用時は、誤変換に注意。

hoge  
***

hoge  
___

hoge  

---

## detail

<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML <em>tags</em> do <b>work</b>, in most cases.</dd>

  <dt>Markdown in HTML</dt>
  <dd>

  Does *not* work **very** well. HTML tags work, in most cases.

  </dd>
  </dl>

<details class="optional-class"><summary>詳細畳み込み</summary><p>内容</p></details>

## Blockquotes 引用

先頭に`>`を記述します。ネストは`>`を多重に記述します。

> 引用  
> 引用 
> > 多重引用  

## Code コード

バッククオート3つ、あるいはダッシュ３つで囲みます。

~~~
#include <stdio.h>

int main(void) {
    printf("Hello world!\n");
    return 0;
}
~~~

``` c
#include <stdio.h>

int main(void) {
    printf("Hello world!\n");
    return 0;
}
```

``` c++
#include <iostream>

int main(void) {
    std::cout << "Hello world!" << std::endl;
    return 0;
}
```
    
```java
public class Hello {
    public static void main(String[] args) {
        System.out.println("Hello, world!!");
        return;
    }
}
```

## pre 整形済みテキスト

半角スペース4個もしくはタブで、コードブロックをpre表示できます

    class Hoge
        def hoge
            print 'hoge'
        end
    end

## コメント

この下にコメントが書いてある。  
<!-- コメント -->

### header1

[return to jump-sample](#jump-sample)  
[return to テキスト装飾](#custom-id)  


## Table 表

#### 拡張記法
`-`と`|`を使ってtableを作成します。

| 左揃え | 中央揃え | 右揃え |
|:---|:---:|---:|
|1 |2<br>7 |3 |
|4 |5 |6 |

#### HTML記法
<table border="1" >
<thead>
<tr>
  <th style="text-align:left">見出し1</th>
  <th style="text-align:center">見出し2</th>
  <th style="text-align:right">見出し3</th>
</tr>
</thead>
<tbody>
<tr>
  <td style="text-align:left">1</td>
  <td style="text-align:center">内容1-2<br>1-7</td>
  <td rowspan=2 style="text-align:right">内容1-3</td>
</tr>
<tr>
  <td colspan=2 style="text-align:center">内容2-1</td>
</tr>
</tbody>
</table>

## Link

[google](http://google.co.jp)

[google](http://google.co.jp "Google top")

http://google.co.jp

<http://google.co.jp/>  

## ファイル指定ジャンプ

[タブにないファイル](../../first/hide.md)

[こちらは飛べない](/first/hide.md)
