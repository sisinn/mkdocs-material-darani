# はじめに

ナレッジをMarkdownで蓄積するシステムの検討  
作成時は、Gitで構成管理し、閲覧時は、Web化して検索も有効にする  

## 採用技術

* mkdocs
* GitLab
