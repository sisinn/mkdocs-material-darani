---
title: ここの記載で、Topに表示される文言が、第１階層の記載から書き換わる
---

## 画像

![中二病](./img/kids_chuunibyou_girl.png)

[![中二病](https://4.bp.blogspot.com/-hko-old-diQ/WdMyjmpC3SI/AAAAAAABHRw/JKqfmklVJDoXQCpdh_ZG-KvZJAfYN0KPwCLcBGAs/s800/kids_chuunibyou_girl.png)](https://www.irasutoya.com/2017/10/blog-post_613.html "いらすとや")

以下は、VSCodeではうまく処理されない  
<img src="https://4.bp.blogspot.com/-hko-old-diQ/WdMyjmpC3SI/AAAAAAABHRw/JKqfmklVJDoXQCpdh_ZG-KvZJAfYN0KPwCLcBGAs/s800/kids_chuunibyou_girl.png” alt="中二病" width="50%">

## PlantUML

以下は、VSCodeでは、別途PlantUMLでプレビュ表示が必要
```plantuml
@startuml
アリス -> ボブ: Authentication Request
ボブ --> アリス: Authentication Response

アリス -> ボブ: Another authentication Request
アリス <-- ボブ: another authentication Response
@enduml
```

```plantuml
@startuml
left to right direction
actor お客様 as g
package Professional {
  actor Chef as c
  actor "Food Critic" as fc
}
package Restaurant {
  usecase "食べる" as UC1
  usecase "支払い" as UC2
  usecase "飲む" as UC3
  usecase "Review" as UC4
}
fc --> UC4
g --> UC1
g --> UC2
g --> UC3
@enduml
```

```plantuml
@startuml

DataAccess - [最初のコンポーネント]
[最初のコンポーネント] ..> HTTP : use

@enduml
```


## 数式

GitLab準拠で、KaTex形式での記載に対応
```math
f(x) = \int_{-\infty}^\infty
    \hat f(\xi)\,e^{2 \pi i \xi x}
    \,d\xi
```   

インラインで数式を書きます。$`E=mc^2`$

This math is inline $`a^2+b^2=c^2`$.

This is on a separate line

```math
a^2+b^2=c^2
```

## 定義リスト

以下は、VSCodeでは、うまく動かない

イチゴ
: バラ科の多年草。食用として供されている部分は花托であり
  果実ではない。イチゴにとっての果実は一見して種子に見える
  一粒一粒であり、正確には痩果という。

## 注釈

以下は、VSCodeでは、うまく動かない

A footnote reference tag looks like this: [^1]

This reference tag is a mix of letters and numbers. [^footnote-42]

[^1]: This is the text inside a footnote.

[^footnote-42]: This is another footnote.

## Admonition

以下は、VSCodeでは、うまく動かない

!!! note
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

## タスクリスト

以下は、VSCodeでは、うまく動かない

- [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
- [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [ ] Praesent sed risus massa
- [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque
